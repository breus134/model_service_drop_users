import pandas as pd
import mlflow
import os
from dotenv import load_dotenv
from fastapi import FastAPI, HTTPException, UploadFile, File

load_dotenv()

app = FastAPI()


class Model:
    def __init__(self, model_name, model_stage):
        """
        Init the model
        :param model_name: Name of the model in registry
        :param model_stage: Stage of the model
        """
        self.model = mlflow.pyfunc.load_model(f"models:/{model_name}/{model_stage}")

    def predict(self, data):
        """
        Use the loaded model to make predict
        :param data: Pandas DataFrame to perform predictions
        :return predictions:
        """
        predictions = self.model.predict(data)
        return predictions


model = Model('drop-users_rf', 'Staging')


@app.post("/invocations")
async def create_upload_file(file: UploadFile = File(...)):
    if file.filename.endswith(".csv"):
        with open(file.filename, "wb") as f:
            f.write(file.file.read())
        data = pd.read_csv(file.filename)
        os.remove(file.filename)
        predictions = list(map(str, list(model.predict(data))))
        return predictions
    else:
        raise HTTPException(status_code=400, detail="Invalid file format. Only CSV file accepted")
